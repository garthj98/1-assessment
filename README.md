# First Academy assessment

## Pre requisits 
- 3 servers (configured as bellow)
  - 2 ubuntu 
  - 1 AWS linux
      - The load balancer should allow port 80 to all and 22 to home IP only
      - Web servers should only allow 22 to home and 80 only to the load balancers private IP adress 

## Steps
- copy repo `git clone git@bitbucket.org:garthj98/1-assessment.git`
- Run `./HAproxy-setup.sh`
  - Input 5 variables, 
  1. public IP of the loadbalancer 
  2. public IP of AWS server
  3. public IP of ubuntu server
  4. private IP of AWS server
  5. private IP of ubuntu server

Make sure scripts have `chmod +x <scriptName>` permissions.

## Steps
- Launch 3 instances 
  - 1 AWS linux - using centOS
    - Amazom Linux 2 AMI (hvm)
    - 1GB RAM
    - 1 vCPU
  - 2 Ubuntu Server 18.04 LTS
    - 1GB RAM
    - 1 vCPU 
- Install Apache on Amazon Linux server 
  - Create a user 
    - Username `instructor`
    - Password `m4g1cAut0L0t10n`
  - Make sure that sshd has been set to allow password log in, and that the firewall allows anywhere to ssh.
  - Configure the home page using an init script in format:
```
#!/bin/bash
### BEGIN INIT INFO
# Provides:       Web page
# Required-Start: $local_fs $network $named $time $syslog
# Required-Stop:  $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:    Builds my servers web page
### END INIT INFO
# description: Set my web page
# chkconfig: 2345 99 99 
case $1 in
    'start')
            echo -e "<html>\nHello there<br>” >/var/www/html/index.html
		echo -e "My IP address is $(ifconfig | grep -A1 eth0 | grep inet | awk '{print $2}'| sed 's/^.*://')\n</html>" >>/var/www/html/index.html
		echo -e "Operating system ……" >>/var/www/html/index.html
		echo -e "Web server is ……… and version ………" >>/var/www/html/index.html
            ;;
    *)
        echo "Not a valid argument"
        ;;
esac
```
- Follow the same steps for one of the Ubuntu Machines
- Set up a HA proxy load balancer on the other Ubuntu machine 
  - Direct to the private IP address of the other 2 servers in a round robin way
  - The load balancer should allow port 80 to all and 22 to home IP only
  - Web servers should only allow 22 to home and 80 only to the load balancers private IP adress 