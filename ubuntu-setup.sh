# Set machine user
USER=ubuntu

# Set key location 
KEYLOC=$1

# Set public IP address 
IP=$2

# Secure login and send commands
ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$IP << EOF

sudo apt update

if sudo nginx -v ; 
then
    echo 'nginx is already installed'
else
    sudo apt install nginx -y
fi

sudo systemctl start nginx 

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>
Hello there
</h1>
<p>
My IP address is $3
</p>
<p>Operating system Ubuntu</p>
<p>Web server is Nginx and version </p>
_END_"

sudo useradd -m instructor
sudo passwd instructor
m4g1cAut0L0t10n
m4g1cAut0L0t10n

#sudo sh -c "cat >>/etc/ssh/sshd_config <<_END_
#AllowUsers  instructor
#_END_"

#sudo systemctl restart sshd

sudo scp /home/ubuntu/.ssh/authorized_keys /home/instructor/

EOF

echo 'Ubuntu configured'