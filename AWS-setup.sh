# Set machine user
USER=ec2-user

# Set key location 
KEYLOC=$1

# Set public IP address 
IP=$2

# Secure login and send commands
ssh -o StrictHostKeyChecking=no -i $KEYLOC $USER@$IP <<EOF

# Update redhat
sudo yum update -y

sudo yum install -y httpd.x86_64

# Start apache
sudo systemctl start httpd.service

sudo systemctl enable httpd.service

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>
Hello there
</h1>
<p>
My IP address is $3
</p>
<p>Operating system Amazon Linux</p>
<p>Web server is ……… and version ………</p>
_END_"

sudo useradd -m instructor
sudo passwd instructor
m4g1cAut0L0t10n
m4g1cAut0L0t10n

sudo scp /home/ubuntu/.ssh/authorized_keys /home/instructor/

EOF

echo 'apache installed'